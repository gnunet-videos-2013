* you broke the internet:

** License and Short Description
Christian Grothoff, Carlo von Lynx, Jacob Appelbaum and Richard
Stallman: "You broke the Internet. We're making ourselves a GNU one."
by Christian Grothoff, Carlo von Lynx, Jacob Appelbaum, and Richard
Stallman is licensed under a
Creative Commons Attribution 3.0 Unported License.

** Long Description
This is the video from the talks given by Christian Grothoff, Carlo
von Lynx, Jacob Appelbaum and Richard Stallman in Berlin on August
1st. The talks are in English, even though the welcoming words
are in German.

*** Disclaimer:
The subject of these talks are GNUnet, Secushare, Internet Censorship
and Surveillance, and Free Software. While the talks were hosted and
recorded by the Pirate Party Berlin, no political statements or
endorsements on behalf of the TU Munich or the GNUnet project or
its sponsors are implied.
